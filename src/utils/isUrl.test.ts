import isUrl from './isUrl';

it('should reject an empty string', function () {
    expect(isUrl("")).toBe(false);
})
it('should reject a non url string', function () {
    expect(isUrl("hello")).toBe(false);
    expect(isUrl("http//a.valid.url")).toBe(false);
    expect(isUrl("jfieowpqj")).toBe(false);
    expect(isUrl("\x10")).toBe(false);
})
it('should reject an non http url', function () {
    expect(isUrl("bolt://example.com")).toBe(false);
    expect(isUrl("ftp://example.com")).toBe(false);
    expect(isUrl("htt://example.com")).toBe(false);
})
it('should accept an http url', function () {
    expect(isUrl("http://example.com")).toBe(true);
    expect(isUrl("http://yay.com")).toBe(true);
    expect(isUrl("https://example.com")).toBe(true);
})