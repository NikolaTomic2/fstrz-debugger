import React from 'react';
import './TopBar.css'

const TopBar: React.FC = ({children}) => {
    return (
        <div className="TopBar">{children}</div>
    );
}

export default TopBar;