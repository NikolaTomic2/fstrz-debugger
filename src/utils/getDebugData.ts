import axios from 'axios';
import isUrl from './isUrl';

const getDebugData = async (url: string, setEntries: Function) => {
    if (!isUrl(url)) return false;
    try {
        const apiBase = process.env.REACT_APP_API_BASE || 'http://localhost:8000';
        const date = new Date();
        const result = await axios.get(`${apiBase}/?url=${url}`);
        const newEntry = {
            url: url,
            time: date,
            debugData: result.data
        };
        const historyEntries = JSON.parse(localStorage.getItem('HistoryEntries') || '[]');
        const newEntries = [newEntry].concat(historyEntries);
        localStorage.setItem('HistoryEntries', JSON.stringify(newEntries));
        setEntries(newEntries);
    } catch (error) {
        console.error(error);
    }
    return true;
}

export default getDebugData;