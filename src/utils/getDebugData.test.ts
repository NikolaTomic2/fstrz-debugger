import getDebugData from './getDebugData';
import sinon from 'sinon';
import axios from 'axios';

it('should refuse an invalid url', async function () {
    expect(await getDebugData('hello', () => {})).toBe(false);
})

it('should try to add a new entry', async function () {
    const returnData = {
        data: {
            statusCode: 204,
            plugged: false
        }
    };
    sinon.stub(axios, 'get').resolves(Promise.resolve(returnData));
    const spy = sinon.spy();
    await getDebugData('https://example.com', spy);
    expect(spy.called).toBe(true);
    expect(spy.lastCall.firstArg[0]['debugData']).toEqual(returnData.data);
})

afterEach(() => {
    sinon.restore();
})