import React from 'react';
import './App.css';
import SideBar from './components/SideBar';
import MainPage from './pages/MainPage';
import logo from './images/fasterize_logo_mini.png';

const App: React.FC = () => {
  return (
    <div className="App">
      <SideBar>
        <img className="logo" src={logo} alt="Fasterize mini logo" width="38" height="24" />
        Fasterize
      </SideBar>
      <div className="content">
        <MainPage />
      </div>
    </div>
  );
}

export default App;
