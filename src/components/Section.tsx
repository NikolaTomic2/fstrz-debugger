import React from 'react';
import Title from './Title';
import './Section.css'

interface SectionProps {
    title: string
}

const Section: React.FC<SectionProps> = ({children, title}) => {
    return (
        <div className="Section">
            <Title>{title}</Title>
            {children}
        </div>
    );
}

export default Section;