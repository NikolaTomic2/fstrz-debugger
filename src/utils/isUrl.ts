const isUrl = (x: string): boolean => {
    try {
        const url = new URL(x);
        return url.protocol === 'http:' || url.protocol === 'https:';
    } catch (_) {
        return false;
    }
}

export default isUrl;