import React, { useState } from 'react';
import TopBar from '../components/TopBar';
import Section from '../components/Section';
import Debug from '../components/Debug';
import History from '../components/History';
import './MainPage.css';

const MainPage: React.FC = () => {
    const [historyEntries, setHistoryEntries] =
        useState(JSON.parse(localStorage.getItem('HistoryEntries') || '[]'));

    return (
        <div className="MainPage">
            <TopBar>
                FASTERIZE DEBUGGER
            </TopBar>
            <div className="MainPageContent">
                <Section title="HEADER DEBUGGER">
                    <Debug setHistoryEntries={setHistoryEntries}/>
                </Section>
                <Section title="HISTORY">
                    <History historyEntries={historyEntries}/>
                </Section>
            </div>
        </div>
    );
}

export default MainPage;