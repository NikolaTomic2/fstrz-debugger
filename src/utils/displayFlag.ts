const displayFlag = (flag: string) => {
    return flag.charAt(0).toUpperCase() + flag.slice(1, 10);
};

export default displayFlag;