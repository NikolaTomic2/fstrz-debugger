import displayFlag from './displayFlag';

it('should return an empty string', function () {
    expect(displayFlag('')).toBe('');
})

it('should capitalize the first letter', function () {
    expect(displayFlag('abc')).toBe('Abc');
    expect(displayFlag('dEf')).toBe('DEf');
    expect(displayFlag('Hij')).toBe('Hij');
})

it('should shorten the flag', function () {
    expect(displayFlag('abcdefghijklmnopqrstuvwxyz')).toBe('Abcdefghij');
})