import React, { useState } from 'react';
import getDebugData from '../utils/getDebugData';
import './Debug.css'

type DebugProps = {
    setHistoryEntries: Function;
};

const Debug: React.FC<DebugProps> = ({ setHistoryEntries }) => {
    const [url, setUrl] = useState('');

    return (
        <div className="Debug">
            <div className="DebugText">Url to check</div>
            <div className="DebugContent">
                <input className="DebugInput" type="text"
                    onChange={(e) => setUrl(e.target.value)}
                    onKeyPress={(e) => e.key === 'Enter' ? getDebugData(url, setHistoryEntries) : null}
                />
                <button className="DebugButton" onClick={() => getDebugData(url, setHistoryEntries)}>LAUNCH ANALYSIS</button>
            </div>
        </div>
    );
}

export default Debug;